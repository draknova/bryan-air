# Bryan Air Liveries

This repository contain all the various Bryan Air Liveries.

Bryan Air is a Virtual Airline operating through the MSFS addon “OnAir Company” software.

## Liveries

Simply paste the folders in your community folder.

You can [download all](https://gitlab.com/draknova/bryan-air/-/archive/master/bryan-air-master.zip) or select a specific one:

| Aircraft | Classic |
| --- |:---:|
| Airbus 320 | [Download](https://gitlab.com/draknova/bryan-air/-/archive/master/bryan-air-master.zip?path=Asobo_A320_BRYAN) |
| Boeing 747 | [Download](https://gitlab.com/draknova/bryan-air/-/archive/master/bryan-air-master.zip?path=Asobo_A747_BRYAN) |
| Boeing 787 | [Download](https://gitlab.com/draknova/bryan-air/-/archive/master/bryan-air-master.zip?path=Asobo_A787_BRYAN) |
| Cessna CJ4 | [Download](https://gitlab.com/draknova/bryan-air/-/archive/master/bryan-air-master.zip?path=Asobo_CJ4_BRYAN) |
| KingAir 350i | [Download](https://gitlab.com/draknova/bryan-air/-/archive/master/bryan-air-master.zip?path=Asobo_KingAir350_BRYAN) |
| Cessna Longitude | [Download](https://gitlab.com/draknova/bryan-air/-/archive/master/bryan-air-master.zip?path=Asobo_Longitude_BRYAN) |


## Participating
Anyone can create or update the liveries. Just create a branch and submit a merge request on the Main.

## Livery Editor
I invite anyone who which to create liveries to use this tool: [MSFS Livery Editor](https://gitlab.com/draknova/msfs-livery-editor) 